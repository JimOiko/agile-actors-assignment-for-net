# Agile Actors Assignment for NET


## Name
Get Weather,News And Nasa Solar Flares Data

## Description
This is an API aggregation service that gathers data from 3 external APIs (openweathermap, newsapi and nasaApi https://api.nasa.gov/) and provides a unified endpoint to access the aggregated information.

## Installation
In order for it to work you need to clone the project and Run It. When you run it it will open a page to https://localhost:7206/swagger/index.html where you will see the main endpoint (/api/Main/allData/get) that return all the unified Data.

## Usage
Now, through swagger you can execute calls to get the responses that the API will serve. If you want to see the weather it is required that you insert correct lat and longtitude, if you want to see the news of a specific country you need to pass the correct countryKeyword (e.g. "us" ,"gr" etc)....
OrderBy field of request is used to sort the data depending on how you desire to see them:
    - OrderBy ="name asc"/ "name desc" will sort the weather forecasts alphabetically ascending or descending accordingly ("Clear SKy" , :Rainy" etc)
    - OrderBy = "date asc"/"date desc" will sort the articles of the country you require in chronological order
Also, using the SearchTitle field you can filter the Articles based on what word you are inserting so that you see more results of what you want

Finally, the startDate, endDate fields are there to specify the time range when the user wants to see when Solar Flares have occured.

The overall Request as shown also on the Swagger is :
```
{
  "orderBy": "string",     //"name asc", "name desc" , "date asc", "date asc"
  "searchTitle": "string", // whatever combination of letters
  "lat": 0,
  "lon": 0,
  "countryKeyword": "string",	//"us","gr" etc.
  "startDate": "string",       // In this format "2016-01-30"
  "endDate": "string" 	       // In this format "2016-01-30"
}
```
The form of the output will be:
```
{
  "weatherData": {
   "coord": {
      "lon": 23.7275,
      "lat": 37.9838
    },
    "main": {
      "temp": 298.79,
      "humidity": 57
    },
    "name": "Metaxourgio",
    "sys": {
      "sunrise": 1716088293,
      "sunset": 1716139913
    },
    "weather": [
      {
        "id": 0,
        "main": "Clear",
        "description": "clear sky"
      }
    ],
    "status": "OK",
    "errorMessage": null
  },
  "newsData": {
      "articles": [
      {
        "title": "Γιωργος Λάνθιμος: Παγκόσμια πρεμιέρα για τη νέα του ταινία στις Κάννες -Δείτε φωτογραφίες από το κόκκινο χαλί -    iefimerida",
        "publishedAt": "0001-01-01T00:00:00",
        "dateOfPublication": "18 May 2024 05:40",
        "author": "iefimerida"
      },
      {
        "title": "Γερμανία: «Πνίγηκε» το Σάαρλαντ -Μεγάλες πλημμύρες μετά από σφοδρές βροχοπτώσεις [βίντεο] - iefimerida",
        "publishedAt": "0001-01-01T00:00:00",
        "dateOfPublication": "18 May 2024 05:06",
        "author": "iefimerida"
      }
    ],
    "totalResults": 2,
    "status": "OK",
    "errorMessage": "null"
  },
  "nasaData": {
    "solarFLares": [
      {
        "beginTime": "2024-04-19T04:40:00Z",
        "endTime": "2024-04-19T05:03:00Z",
        "link": "https://webtools.ccmc.gsfc.nasa.gov/DONKI/view/FLR/30136/-1",
        "beginDateOfFLare": "19 April 2024 04:40"
      },
      {
        "beginTime": "2024-04-19T12:53:00Z",
        "endTime": "2024-04-19T13:23:00Z",
        "link": "https://webtools.ccmc.gsfc.nasa.gov/DONKI/view/FLR/30139/-1",
        "beginDateOfFLare": "19 April 2024 12:53"
      }
    ],
    "status": "OK",
    "errorMessage": null
  }
}
```


## Project status
Project is fully completed with all the test from Unit Testing passed.
All the Api Keys have been inserted as environmental variables for more safety. 
