﻿using AA_Assignment.Models;
using System.Text.Json;
using AA_Assignment.Interfaces;
using System.Net.Http;

namespace AA_Assignment.WeatherService
{
    public class NewsService: INewsService
    {
        private readonly HttpClient _httpClient;

        public NewsService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();

        }

        public async Task<NewsMdl> GetNewsAsync(RequestMdl req)
        {
            NewsMdl info = null;
            try
            {
                string url = $"https://newsapi.org/v2/top-headlines?country={req.CountryKeyword}&apiKey={Environment.GetEnvironmentVariable("NEWSAPIKEY")}";
                var response = await _httpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync();
                info = JsonSerializer.Deserialize<NewsMdl>(json);
                if (info != null && info.articles != null)
                {
                    if (!String.IsNullOrEmpty(req.SearchTitle) && req.SearchTitle != "string")    //Search what titles you want to see
                    {
                        info.articles = info.articles.Where(w => w.title.Contains(req.SearchTitle)).ToList();
                    }
                    if (req.OrderBy == "date asc") //OrderBy based on the name of the weather Rain, CLear Sky etc
                        info.articles = info.articles.OrderBy(o => o.publishedAt).ToList();
                    else if (req.OrderBy == "date desc")
                        info.articles = info.articles.OrderByDescending(o => o.publishedAt).ToList();
                }

                //transform date to a more readable one
                info.articles = info.articles.Select(article => new Article()
                {
                    title = article.title,
                    DateOfPublication = article.publishedAt.ToString("d MMMM yyyy HH:mm"),
                    author = article.author
                }).ToList();
                info.totalResults = info.articles.Count;
                return info;
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., logging)
                Console.WriteLine($"Error fetching news data: {ex.Message}");
                return null;
            }
        }
    }
}
