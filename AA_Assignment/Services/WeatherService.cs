﻿using AA_Assignment.Models;
using System.Text.Json;
using AA_Assignment.Interfaces;

namespace AA_Assignment.WeatherService
{
    public class WeatherService: IWeatherService
    {
        private readonly HttpClient _httpClient;

        public WeatherService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();
        }

        public async Task<WeatherMdl> GetWeatherAsync(RequestMdl req)
        {
            WeatherMdl info = null;
            try
            {
                string url = $"https://api.openweathermap.org/data/2.5/weather?lat={req.Lat}&lon={req.Lon}&appid={Environment.GetEnvironmentVariable("WEATHERAPIKEY")}";
                var response = await _httpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync();
                info = JsonSerializer.Deserialize<WeatherMdl>(json);

                if (req.OrderBy == "name asc")
                    info.weather = info.weather.OrderBy(s => s.main).ToList();
                else if(req.OrderBy == "name desc")
                    info.weather = info.weather.OrderByDescending(s => s.main).ToList();

                return info;
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., logging)
                Console.WriteLine($"Error fetching weather data: {ex.Message}");
                return null;
            }
        }
    }
}
