﻿using AA_Assignment.Models;
using System.Text.Json;
using AA_Assignment.Interfaces;
using System.Net.Http;

namespace AA_Assignment.WeatherService
{
    public class NasaService: INasaService
    {
        private readonly HttpClient _httpClient;

        public NasaService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();

        }

        public async Task<List<SolarFlareMdl>> GetSolarFlaresAsync(RequestMdl req)
        {
            List<SolarFlareMdl> info = null;
            try
            {
                string url = $"https://api.nasa.gov/DONKI/FLR?startDate={req.StartDate}&endDate={req.EndDate}&api_key={Environment.GetEnvironmentVariable("NASAAPIKEY")}";
                var response = await _httpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync();
                info = JsonSerializer.Deserialize<List<SolarFlareMdl>>(json);
                
                if (req.OrderBy == "date asc") //OrderBy based on the name of the weather Rain, CLear Sky etc
                    info = info.OrderBy(o => o.beginTime).ToList();
                else if (req.OrderBy == "date desc")
                    info= info.OrderByDescending(o => o.beginTime).ToList();

                //transform date to a more readable one
                info = info.Select(sf => new SolarFlareMdl()
                {
                    beginTime = sf.beginTime,
                    endTime =  sf.endTime,
                    link = sf.link,
                    BeginDateOfFLare = sf.beginTime.ToString("d MMMM yyyy HH:mm"),
                }).ToList();
                return info;
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., logging)
                Console.WriteLine($"Error fetching flare data: {ex.Message}");
                return null;
            }
        }
    }
}
