﻿using AA_Assignment.Models;

namespace AA_Assignment.Interfaces
{
    public interface INewsInterface
    {
        Task<NewsMdl> GetNews(RequestMdl req);
    }
}
