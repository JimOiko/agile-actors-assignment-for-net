﻿using AA_Assignment.Models;

namespace AA_Assignment.Interfaces
{
    public interface IWeatherInterface
    {
        Task<WeatherMdl> GetWeather(RequestMdl req);
    }
}
