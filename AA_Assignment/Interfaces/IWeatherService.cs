﻿using AA_Assignment.Models;

namespace AA_Assignment.Interfaces
{
    public interface IWeatherService
    {
        Task<WeatherMdl> GetWeatherAsync(RequestMdl req);
    }
}
