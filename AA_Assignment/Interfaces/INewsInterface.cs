﻿using AA_Assignment.Models;

namespace AA_Assignment.Interfaces
{
    public interface INewsService
    {
        Task<NewsMdl> GetNewsAsync(RequestMdl req);
    }
}
