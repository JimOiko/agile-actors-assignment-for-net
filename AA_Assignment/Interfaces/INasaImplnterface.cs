﻿using AA_Assignment.Models;

namespace AA_Assignment.Interfaces
{
    public interface INasaInterface
    {
        Task<SolarFlareResponse> GetSolarFlares(RequestMdl req);
    }
}
