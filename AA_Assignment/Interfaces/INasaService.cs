﻿using AA_Assignment.Models;

namespace AA_Assignment.Interfaces
{
    public interface INasaService
    {
        Task<List<SolarFlareMdl>> GetSolarFlaresAsync(RequestMdl req);
    }
}
