using AA_Assignment.Implementation;
using AA_Assignment.Interfaces;
using AA_Assignment.WeatherService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IWeatherInterface, WeatherImp>();
builder.Services.AddScoped<INewsInterface, NewsImp>();
builder.Services.AddScoped<INasaInterface, NasaImp>();
builder.Services.AddScoped<IWeatherService, WeatherService>();
builder.Services.AddScoped<INewsService, NewsService>();
builder.Services.AddScoped<INasaService, NasaService>();
builder.Services.AddHttpClient();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
