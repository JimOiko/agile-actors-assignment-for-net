﻿using System.Net;
using AA_Assignment.Implementation;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Nodes;
using AA_Assignment.WeatherService;


namespace AA_Assignment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainController : ControllerBase
    {
        protected IWeatherInterface _weatherImp;
        protected INewsInterface _newsImp;
        protected INasaInterface _nasaImp;
        public MainController(IWeatherInterface weatherImp, INewsInterface newsImp, INasaInterface nasaImp)
        {
            _weatherImp = weatherImp;
            _newsImp = newsImp;
            _nasaImp = nasaImp;
        }

        #region GET All Data
        [HttpPost]
        [Route("allData/get")]
        public async Task<IActionResult> GetAllData(RequestMdl model)
        {
           
            var weatherTask = _weatherImp.GetWeather(model);
            var newsTask = _newsImp.GetNews(model);
            var nasaTask = _nasaImp.GetSolarFlares(model);

            await Task.WhenAll(weatherTask, newsTask);

            var combinedResult = new
            {
                WeatherData = weatherTask.Result,
                NewsData = newsTask.Result,
                NasaData = nasaTask.Result
            };

            // Return the data as a JSON response
            return Ok(combinedResult);
        }
        #endregion 
    }
}
