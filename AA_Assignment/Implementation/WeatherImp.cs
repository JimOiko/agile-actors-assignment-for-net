﻿using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;

namespace AA_Assignment.Implementation
{
    public class WeatherImp : IWeatherInterface
    {
        private readonly IWeatherService _weatherService;

        public WeatherImp(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }
        public async Task<WeatherMdl> GetWeather(RequestMdl req)
        {
            if (req.Lat == null && req.Lon== null)
            {
                req.Lat = 37.983810;
                req.Lon = 23.727539; // Example coordinates for Athens Metaxourgeio
            }
            var weather = await _weatherService.GetWeatherAsync(req);

            if (weather != null)
            {
                Console.WriteLine($"Weather in {weather}: ");
            }
            else
            {
                weather = new WeatherMdl() { ErrorMessage = "Error when fetching weather Data" };
            }

            return  weather;
        }
    }
}
