﻿using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;
using AA_Assignment.WeatherService;

namespace AA_Assignment.Implementation
{
    public class NewsImp : INewsInterface
    {
        private readonly INewsService _newsService;

        public NewsImp(INewsService newsService)
        {
            _newsService = newsService;
        }

        #region  Get
        public async Task<NewsMdl> GetNews(RequestMdl req)
        {

            var httpClient = new HttpClient();

            if (String.IsNullOrEmpty(req.CountryKeyword) || req.CountryKeyword == "string")
            {
                req.CountryKeyword = "gr"; 
            }
            var news = await _newsService.GetNewsAsync(req);

            if (news != null)
            {
                Console.WriteLine($"News are {news}: ");
            }
            else
            {
                news = new NewsMdl() { ErrorMessage = "Error when fetching news Data" };
            }

            return  news;
        }
        #endregion

    }
}
