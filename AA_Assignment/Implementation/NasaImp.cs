﻿using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;
using AA_Assignment.WeatherService;

namespace AA_Assignment.Implementation
{
    public class NasaImp : INasaInterface
    {
        private readonly INasaService _nasaService;

        public NasaImp(INasaService nasaService)
        {
            _nasaService = nasaService;
        }

        #region  Get Solar Flares
        public async Task<SolarFlareResponse> GetSolarFlares(RequestMdl req)
        {

            var httpClient = new HttpClient();

            var solarFlares = await _nasaService.GetSolarFlaresAsync(req);

            var response = new SolarFlareResponse();

            if (solarFlares != null)
            {
                response.SolarFLares = solarFlares;
                response.Status = "OK";
            }
            else
            {
                response.SolarFLares = new List<SolarFlareMdl>();
                response.Status = "Error";
                response.ErrorMessage = "Error when fetching NASA data.";
            }

            return response;
        }
        #endregion

    }
}
