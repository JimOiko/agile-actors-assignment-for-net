﻿using Microsoft.Extensions.Options;
using System.Globalization;
using System.Xml.XPath;

namespace AA_Assignment.Models
{
    public class WeatherMdl: BaseMdl
    {
        public WeatherMdl()
        {
            coord = new Coord();
            main = new Temp();
            weather = new List<Weather>();
        }
        public Coord coord { get; set; }
        public Temp main  {get; set; }
        public string name { get; set; }
        public sys sys { get; set; }
        public List<Weather> weather { get; set; }
    }

    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class sys
    {
        public long sunrise { get; set; }
        public long sunset { get; set; }
    }

    public class Weather
    {
        public int Id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
    }

    public class Temp
    {
        public double temp { get; set; }
        public double humidity { get; set; }
    }
}
