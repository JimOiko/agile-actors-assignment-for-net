﻿namespace AA_Assignment.Models
{
    public class NewsMdl : BaseMdl
    {
        public List<Article> articles { get; set; }
        public int totalResults { get; set; }

    }

    public class Article
    {
        public string title { get; set; }
        public DateTime publishedAt { get; set; }
        public string DateOfPublication{ get; set; }
        public string author { get; set; }
    }

}
