﻿namespace AA_Assignment.Models
{
    public class RequestMdl
    {
        public string OrderBy { get; set; }
        public string SearchTitle { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string CountryKeyword { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
