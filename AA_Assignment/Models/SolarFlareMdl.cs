﻿namespace AA_Assignment.Models
{
    public class SolarFlareResponse : BaseMdl
    {
        public SolarFlareResponse()
        {
            SolarFLares = new List<SolarFlareMdl>();
        }

        public List<SolarFlareMdl> SolarFLares { get; set; }
    }
    public class SolarFlareMdl
    {
        public DateTime beginTime { get; set; }
        public DateTime endTime { get; set; }
        public string link { get; set; }
        public string BeginDateOfFLare { get; set; }
    }

}
