﻿using AA_Assignment.Implementation;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AA_Assignment.UnitTests
{
    [TestClass]
    public class NasaImpTests
    {
        private readonly Mock<INasaService> _mockNasaService;
        private readonly NasaImp _nasaImp;

        public NasaImpTests()
        {
            _mockNasaService = new Mock<INasaService>();
            _nasaImp = new NasaImp(_mockNasaService.Object);
        }

        [TestMethod]
        public async Task GetSolarFlares_Returns_WhenStartEndDateIsProvided()
        {
            // Arrange
            var request = new RequestMdl { StartDate = "2021-10-30", EndDate= "2022-12-30" };
            var expectedSolarFlares = new List<SolarFlareMdl>()
            {
                new SolarFlareMdl
                {
                    link = "http://www.dolarflare",
                    beginTime = DateTime.Now.AddDays(-1)
                },
                new SolarFlareMdl
                {
                    link = "http://www.dolarflare2",
                    beginTime = DateTime.Now.AddDays(-3)
                },
            };

            _mockNasaService
                .Setup(service => service.GetSolarFlaresAsync(It.IsAny<RequestMdl>()))
                .ReturnsAsync(expectedSolarFlares);

            // Act
            var result = await _nasaImp.GetSolarFlares(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedSolarFlares, result.SolarFLares);
        }

        [TestMethod]
        public async Task GetNews_ReturnsNews_WhenInCorrectLatLonIsProvided()
        {
            // Arrange
            var request = new RequestMdl { StartDate = null, EndDate = null };
            List<SolarFlareMdl> expectedSolarFlares = null;

            _mockNasaService
                .Setup(service => service.GetSolarFlaresAsync(It.IsAny<RequestMdl>()))
                .ReturnsAsync(expectedSolarFlares);

            // Act
            var result = await _nasaImp.GetSolarFlares(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorMessage, "Error when fetching NASA data.");
        }
    }
}
