﻿using AA_Assignment.Implementation;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AA_Assignment.UnitTests
{
    [TestClass]
    public class WeatherImpTests
    {
        private readonly Mock<IWeatherService> _mockWeatherService;
        private readonly WeatherImp _weatherImp;

        public WeatherImpTests()
        {
            _mockWeatherService = new Mock<IWeatherService>();
            _weatherImp = new WeatherImp(_mockWeatherService.Object);
        }

        [TestMethod]
        public async Task GetWeather_Returns_WhenLatLonIsProvided()
        {
            // Arrange
            var request = new RequestMdl { Lat = 37.983810, Lon = 23.727539 };
            var expectedWeather = new WeatherMdl
            {
                main = new Temp()
                {
                    temp = 122.5,
                    humidity = 45
                },
                coord = new Coord()
                {
                    lat = 37.983810,
                    lon = 23.727539
                }
            };

            _mockWeatherService
                .Setup(service => service.GetWeatherAsync(It.IsAny<RequestMdl>()))
                .ReturnsAsync(expectedWeather);

            // Act
            var result = await _weatherImp.GetWeather(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedWeather, result);
        }

        [TestMethod]
        public async Task GetNews_ReturnsNews_WhenInCorrectLatLonIsProvided()
        {
            // Arrange
            var request = new RequestMdl { Lat = 12343432432423432434, Lon = 4323543546546767658};
            WeatherMdl expectedWeather = null;

            _mockWeatherService
                .Setup(service => service.GetWeatherAsync(It.IsAny<RequestMdl>()))
                .ReturnsAsync(expectedWeather);

            // Act
            var result = await _weatherImp.GetWeather(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorMessage, "Error when fetching weather Data");
        }
    }
}
