﻿using AA_Assignment.Implementation;
using AA_Assignment.Interfaces;
using AA_Assignment.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AA_Assignment.UnitTests
{
    [TestClass]
    public class NewsImpTests
    {
        private readonly Mock<INewsService> _mockNewsService;
        private readonly NewsImp _newsImp;

        public NewsImpTests()
        {
            _mockNewsService = new Mock<INewsService>();
            _newsImp = new NewsImp(_mockNewsService.Object);
        }

        [TestMethod]
        public async Task GetNews_ReturnsNews_WhenCountryKeywordIsProvided()
        {
            // Arrange
            var request = new RequestMdl { CountryKeyword = "us" };
            var expectedNews = new NewsMdl { totalResults = 2,
                articles = new List<Article>
                {
                    new Article
                    {
                        title = "Title 1",
                        publishedAt = DateTime.Now.AddDays(-1)
                    },
                    new Article
                    {
                        title = "Title 2",
                        publishedAt = DateTime.Now.AddDays(-2)
                    }
                }
            };

            _mockNewsService
                .Setup(service => service.GetNewsAsync(It.IsAny<RequestMdl>()))
                .ReturnsAsync(expectedNews);

            // Act
            var result = await _newsImp.GetNews(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedNews, result);
        }

        [TestMethod]
        public async Task GetNews_ReturnsNews_WhenInCorrectCountryKeywordIsProvided()
        {
            // Arrange
            var request = new RequestMdl { CountryKeyword = "qqq" };
            NewsMdl expectedNews = null;

            _mockNewsService
                .Setup(service => service.GetNewsAsync(It.IsAny<RequestMdl>()))
                .ReturnsAsync(expectedNews);

            // Act
            var result = await _newsImp.GetNews(request);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.ErrorMessage, "Error when fetching news Data");
        }
    }
}
